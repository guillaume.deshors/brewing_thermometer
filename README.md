This project represents the Arduino code for a multi-function thermometer meant to help brewing beer.

The arduino is connected to :
 - a LM35 temperature sensor (pin A0)
 - a servomotor (pin 11)
 - a buzzer (pin 13)
 - 3 push buttons (pins 12,4, 0 for set, more, less)
 - a 3-state button taken from a hairdryer, giving us the selection between the 3 modes of operation (pins 5 and 7)

The wiring is very simple, I didn't make a schematics.

The servomotor controls a lever that presses on a pipe, acting as an analog valve to control the amount of wort passing through. The hot wort that needs to be coolen down rapidly (from 98°C to about 25°C) passes through a heat exchanger that has a fixed flux of cold water in the other direction. The more slowly the wort passes, the colder it gets out into the fermentor.

The LM35 sensor is at the end of a wire and attached to a small copper pipe through wich the wort also passes.

The more interesting mode is the PID regulator, that opens or closes the valve depending on the temperature of the beer, with the objective of filling the fermentor with a temperature-controlled wort beacuse the yeast die if put in a liquid too hot. In this mode the + and - buttons change the value of, at choice : 
 - the desired temperature
 - the Kp value
 - the Ki value
 - the Kd value

Pressing "set" alternates between these 4 modes, a long press saves these value in the EEPROM for future use.

Here is an image of the device in operation :

![device in operation](doc/device_in_operation.jpg)

Two more modes exist : one is called "manual" and allows to open or close the valve with the buttons, and an "alarm" mode that is a temperature display with upper and lower alarms (hence the buzzer), useful at various moments in the brewing or fermenting process.

At this stage the only way to use the device is with a computer plugged in, through serial console, but I'll add a display (oled or 1602) soon to use it in a standalone way.
