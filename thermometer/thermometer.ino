#include <AceButton.h>
// https://github.com/bxparks/AceButton
#include <PID_v1.h>

#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <Adafruit_TiCoServo.h>

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <OneWire.h>
#include <DallasTemperature.h>

#define VERSION "1.2"

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
// The pins for I2C are defined by the Wire-library.
// On an arduino UNO:       A4(SDA), A5(SCL)
// On an arduino MEGA 2560: 20(SDA), 21(SCL)
// On an arduino LEONARDO:   2(SDA),  3(SCL), ...
#define OLED_RESET 4        // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

const boolean serial = true;

using namespace ace_button;

// The pin number attached to the button.
const int BUTTON_SET_PIN = 12;
const int BUTTON_MORE_PIN = 4;
const int BUTTON_LESS_PIN = 0;

const int BUTTON_MODE2_PIN = 5;
const int BUTTON_MODE3_PIN = 7;

const int BUZZER_PIN = LED_BUILTIN;

const int THERMOMETER_PIN = A0; // pin for LM35

#define ONE_WIRE_BUS 2 // pin for thermometer bs18b20

const int SERVO_PIN = 10;

#define MODE_THERMOMETER 1
#define MODE_REGULATOR 2
#define MODE_MANUAL 3
unsigned short mode;

// LED states. Some microcontrollers wire their built-in LED the reverse.
const int LED_ON = HIGH;
const int LED_OFF = LOW;

// Both buttons automatically use the default System ButtonConfig. The
// alternative is to call the AceButton::init() method in setup() below.
AceButton buttonSet(BUTTON_SET_PIN);
AceButton buttonMore(BUTTON_MORE_PIN);
AceButton buttonLess(BUTTON_LESS_PIN);

ButtonConfig modeConfig;
AceButton buttonMode2(&modeConfig);
AceButton buttonMode3(&modeConfig);

// Setup a oneWire instance to communicate with any OneWire devices
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

// choose between LM35 and BS18B20
bool bs18b20 = true;

// variables for tune
double *value;
unsigned long tuneStart;
double *min;
double *max;

// value for manual mode
double servo_min = 18;
double servo_max = 175;
double servo_value = servo_max;

// variables for PID
double K_min = 0.01;
double K_max = 100;

struct config_t
{
  double Kp = 4;
  double Ki = K_min;
  double Kd = K_min;
} conf;

double target_temp = 25;
double temp_min = 1;
double temp_max = 100;

// actual temperature
double temperature;

// servo
Adafruit_TiCoServo myservo; // create servo object to control a servo

#define REG_MODE_TEMP 1
#define REG_MODE_KP 2
#define REG_MODE_KI 3
#define REG_MODE_KD 4
unsigned short reg_mode = REG_MODE_TEMP;

/* https://playground.arduino.cc/Code/PIDLibrarySetControllerDirection/
If my Input is above Setpoint, should the output be increased or decreased?
Depending on what the PID is connected to, either could be true. With a car,
the output should be decreased to bring the speed down. For a refrigerator,
the opposite is true. The output (cooling) needs to be increased to bring my
temperature down.

Here : servo - is closed ; servo + is open
      - is colder (less flux, thus cooler) ; + is hotter
      --> our system works like an OVEN and should be DIRECT !!!
*/

// PID - we'll set the Ks later
PID myPID(&temperature, &servo_value, &target_temp, 0, 0, 0, DIRECT);

// alarm
boolean alarm_ringing = false;      // ringing right now
boolean alarm_buzzer_state = false; // used to modulate the buzzer
double alarm_low = 10;
boolean alarm_low_on = true; // is there a low limit
double alarm_high = 25;
boolean alarm_high_on = true; // is there a high limit
#define ALARM_HIGH 0
#define ALARM_LOW 1
int alarm_mode = ALARM_HIGH; // are we setting low or high limit

void handleModeEvent(AceButton *, uint8_t, uint8_t);

void handleModeEvent(AceButton *button, uint8_t eventType,
                     uint8_t /* buttonState */)
{
  switch (eventType)
  {
  case AceButton::kEventPressed:
    if (button->getPin() == BUTTON_MODE2_PIN)
    {
      setMode(MODE_REGULATOR);
    }
    if (button->getPin() == BUTTON_MODE3_PIN)
    {
      setMode(MODE_MANUAL);
    }
    break;
  case AceButton::kEventReleased:
    if (button->getPin() == BUTTON_MODE2_PIN)
    {
      setMode(MODE_THERMOMETER);
    }
    if (button->getPin() == BUTTON_MODE3_PIN)
    {
      setMode(MODE_REGULATOR);
    }
    break;
  }
}

void setMode(unsigned short _mode)
{
  mode = _mode;

  if (mode == MODE_MANUAL)
  {
    if (serial)
      Serial.println("Mode manuel");

    alarm_ringing = false;

    value = &servo_value;
    min = &servo_min;
    max = &servo_max;

    // stop PID
    myPID.SetMode(MANUAL);
  }

  if (mode == MODE_REGULATOR)
  {
    if (serial)
      Serial.println("Mode régulateur");

    alarm_ringing = false;

    if (reg_mode == REG_MODE_TEMP)
    {
      value = &target_temp;
      min = &temp_min;
      max = &temp_max;
      if (serial)
        Serial.println("****** Réglage de Température de consigne ******");
    }
    else
    {
      min = &K_min;
      max = &K_max;
    }
    if (reg_mode == REG_MODE_KP)
    {
      value = &conf.Kp;
      if (serial)
        Serial.println("****** Réglage de Kp ******");
    }
    if (reg_mode == REG_MODE_KI)
    {
      value = &conf.Ki;
      if (serial)
        Serial.println("****** Réglage de Ki ******");
    }
    if (reg_mode == REG_MODE_KD)
    {
      value = &conf.Kd;
      if (serial)
        Serial.println("****** Réglage de Kd ******");
    }
    // start PID
    myPID.SetMode(AUTOMATIC);
  }

  if (mode == MODE_THERMOMETER)
  {
    // stop PID
    myPID.SetMode(MANUAL);
    if (serial)
      Serial.println("Mode thermomètre");

    if (alarm_mode == ALARM_HIGH)
      value = &alarm_high;
    else
      value = &alarm_low;
    min = &temp_min;
    max = &temp_max;
  }
}

void handleButtonEvent(AceButton *, uint8_t, uint8_t);

void setup()
{
  Serial.begin(9600);
  while (!Serial)
    ; // Wait until Serial is ready - Leonardo/Micro
  Serial.println(F("Thermomètre multi-fonctions - la suite en 115200 bps"));
  delay(300);

  Serial.begin(115200);
  while (!Serial)
    ; // Wait until Serial is ready - Leonardo/Micro
  if (serial)
    Serial.println(F("setup(): begin"));

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS))
  {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ; // Don't proceed, loop forever
  }

  // thermal probe
  sensors.begin();
  sensors.setWaitForConversion(false);
  sensors.setResolution(10);

  // Initialize built-in LED as an output.
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(SERVO_PIN, OUTPUT);

  // Buttons use the built-in pull up register.
  pinMode(BUTTON_SET_PIN, INPUT_PULLUP);
  pinMode(BUTTON_MORE_PIN, INPUT_PULLUP);
  pinMode(BUTTON_LESS_PIN, INPUT_PULLUP);

  pinMode(BUTTON_MODE2_PIN, INPUT_PULLUP);
  pinMode(BUTTON_MODE3_PIN, INPUT_PULLUP);

  buttonMode2.init(BUTTON_MODE2_PIN);
  buttonMode3.init(BUTTON_MODE3_PIN);

  // Configure the ButtonConfig with the event handler, and enable all higher
  // level events.
  ButtonConfig *buttonConfig = ButtonConfig::getSystemButtonConfig();
  buttonConfig->setEventHandler(handleButtonEvent);
  buttonConfig->setFeature(ButtonConfig::kFeatureClick);
  buttonConfig->setFeature(ButtonConfig::kFeatureLongPress);
  buttonConfig->setFeature(ButtonConfig::kFeatureRepeatPress);

  // Configs for the tune-up and tune-down buttons. Need RepeatPress instead of
  // LongPress.
  modeConfig.setEventHandler(handleModeEvent);
  modeConfig.setFeature(ButtonConfig::kFeatureClick);
  // These suppressions not really necessary but cleaner.
  modeConfig.setFeature(ButtonConfig::kFeatureSuppressAfterClick);

  // Check if the button was pressed while booting
  if (buttonMode3.isPressedRaw())
  {
    setMode(MODE_MANUAL);
  }
  else if (buttonMode2.isPressedRaw())
  {
    setMode(MODE_REGULATOR);
  }
  else
  {
    setMode(MODE_THERMOMETER);
  }

  // setup PID
  myPID.SetOutputLimits(servo_min, servo_max);
  myPID.SetSampleTime(1000);

  // read stored configuration
  EEPROM_readAnything(0, conf);
  myPID.SetTunings(conf.Kp, conf.Ki, conf.Kd);

  // internal ref. for analog input
  analogReference(INTERNAL);

  myservo.attach(SERVO_PIN);

  if (serial)
    Serial.println(F("setup(): ready"));
}

unsigned long timer_1000 = 0;
unsigned long timer_300 = 0;

void checkButtons()
{
  buttonSet.check();
  buttonMore.check();
  buttonLess.check();
  buttonMode2.check();
  buttonMode3.check();
}

double sumtemp;
int nbtempreadings;

void loop()
{
  // Should be called every 4-5ms or faster, for the default debouncing time
  // of ~20ms.
  checkButtons();

  unsigned long now = millis();

  if (!bs18b20)
  {
    // temperature reading : do it multiple times so we get a steadier mean
    int valeur_brute = analogRead(THERMOMETER_PIN);
    sumtemp = sumtemp + valeur_brute * (1.1 / 1023.0 * 100.0);
    nbtempreadings = nbtempreadings + 1;
  }

  if (now > timer_300 + 300)
  {
    if (alarm_ringing)
    {
      alarm_buzzer_state = !alarm_buzzer_state;
      digitalWrite(BUZZER_PIN, alarm_buzzer_state);
    }
    timer_300 = now;
  }

  // every 1s
  if (now > timer_1000 + 1000)
  {
    timer_1000 = now; // to reinit timer

    // write to servo
    myservo.write(servo_value);

    if (bs18b20)
    {
      // bs18b20 : get a reading
      temperature = sensors.getTempCByIndex(0);
    }
    else
    {
      // compute temperatur from mean of all readings
      temperature = sumtemp / nbtempreadings;
      nbtempreadings = 0;
      sumtemp = 0;
    }

    // handle the alarm
    if (mode == MODE_THERMOMETER || mode == MODE_REGULATOR)
    {
      alarm_ringing = false;
      if (alarm_high_on && temperature > alarm_high)
      {
        alarm_ringing = true;
      }
      if (alarm_low_on && temperature < alarm_low)
      {
        alarm_ringing = true;
      }
    }
    if (!alarm_ringing)
    {
      digitalWrite(BUZZER_PIN, LOW);
    }

    // get temperature requested last time
    if (serial)
      Serial.print("Temperature : ");
    if (serial)
      Serial.println(temperature);

    if (bs18b20)
    {
      // and request a new one
      sensors.requestTemperatures(); // Send the command to get temperatures
    }

    // PID cycle !
    myPID.Compute();
    // display all
    updateDisplay();
  }
}

void updateDisplay()
{
  display.clearDisplay();

  display.setTextSize(3);              // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(44, 0);            // Start at top-left corner

  // display.print("temp : ");
  display.print(String(temperature, 1));
  display.setTextSize(1);
  display.cp437(true);
  display.write(167);
  display.print("C");

  // mode
  // display.setCursor(0, 24);
  display.setCursor(0, 0);
  if (mode == MODE_REGULATOR)
  {
    display.print("Regul.");
    // medium linedisplay.setCursodisplay.setCursor(0, 24);r(0, 24);
    display.setCursor(0, 10);
    display.print((reg_mode == REG_MODE_TEMP) ? "*" : " ");
    display.print("T ");
    display.print(String(target_temp, 1));
    // bottom line
    display.setCursor(0, 24);
    display.print((reg_mode == REG_MODE_KP) ? "*" : "");
    display.print("Kp");
    display.print(String(conf.Kp, 2));
    display.print((reg_mode == REG_MODE_KI) ? " *" : " ");
    display.print("Ki");
    display.print(String(conf.Ki, 2));
    display.print((reg_mode == REG_MODE_KD) ? " *" : " ");
    display.print("Kd");
    display.print(String(conf.Kd, 2));
  }
  if (mode == MODE_THERMOMETER)
  {
    display.print("Therm.");
    display.setCursor(0, 24);
    display.print((alarm_mode == ALARM_LOW) ? "*bas:" : " bas:");
    display.print(alarm_low_on ? String(alarm_low, 1) : "off ");
    display.print((alarm_mode == ALARM_HIGH) ? " *haut:" : "  haut:");
    display.print(alarm_high_on ? String(alarm_high, 1) : "off ");
  }
  if (mode == MODE_MANUAL)
  {
    display.print("Manuel");
    display.setCursor(0, 24);
    display.print("Ouverture : ");
    // calc. open percentage
    double percent = (servo_value - servo_min) / (servo_max - servo_min) * 100;
    display.print(String(percent, 0));
    display.print("%");
  }

  // Show the display buffer on the screen. You MUST call display() after
  // drawing commands to make them visible on screen!
  display.display();
}

// The event handler for both buttons.
void handleButtonEvent(AceButton *button, uint8_t eventType, uint8_t buttonState)
{
  switch (eventType)
  {

  case AceButton::kEventClicked:
    if (button->getPin() == BUTTON_MORE_PIN)
    {

      if (*value == 0)
      {
        *value = *min;
      }
      else
      {
        *value = *value * 1.01;
        if (*value > *max)
        {
          *value = *max;
        }
      }

      if (serial)
        Serial.println(*value);
    }
    if (button->getPin() == BUTTON_LESS_PIN)
    {
      if (*value == *min)
      {
        *value = 0;
      }
      else
      {
        *value = *value * .99;
        if (*value < *min)
        {
          *value = *min;
        }
      }
      if (serial)
        Serial.println(*value);
    }
    if (button->getPin() == BUTTON_SET_PIN)
    {
      if (mode == MODE_REGULATOR)
      {
        reg_mode = reg_mode % 4 + 1;
        setMode(mode);
      }
      else if (mode == MODE_THERMOMETER)
      {

        if (alarm_mode == ALARM_LOW)
        {
          alarm_mode = ALARM_HIGH;
          value = &alarm_high;
          if (serial)
            Serial.println("Alarm set high point");
        }
        else
        {
          alarm_mode = ALARM_LOW;
          value = &alarm_low;
          if (serial)
            Serial.println("Alarm set low point");
        }
      }
    }
    break;

  case AceButton::kEventLongPressed:
    if (button->getPin() == BUTTON_SET_PIN)
    {

      if (mode == MODE_REGULATOR)
      {
        if (serial)
          Serial.println(F("*** enregistrement des paramètres ***"));
        // save configuration
        EEPROM_writeAnything(0, conf);
      }
      else if (mode == MODE_THERMOMETER)
      {
        if (serial)
          Serial.print("Alarm ");
        if (alarm_mode == ALARM_HIGH)
        {
          if (serial)
            Serial.print("high ");
          alarm_high_on = !alarm_high_on;
          if (serial)
            Serial.println(alarm_high_on);
        }
        else
        {
          if (serial)
            Serial.print("low ");
          alarm_low_on = !alarm_low_on;
          if (serial)
            Serial.println(alarm_low_on);
        }
      }
      else
      { // MANUAL MODE
        Serial.print("version ");
        Serial.println(VERSION);

        display.clearDisplay();
        display.setCursor(0, 0);
        display.print("Version ");
        display.print(VERSION);

        // inversion of bs18b20 state
        bs18b20 = !bs18b20;

        display.setCursor(0, 24);
        display.print(bs18b20 ? "Capteur : BS18B20" : "Capteur : LM35");

        display.display();
        delay(2000);
      }
    }
    if (button->getPin() == BUTTON_MORE_PIN || button->getPin() == BUTTON_LESS_PIN)
    {
      tuneStart = millis();
    }
    break;
  case AceButton::kEventRepeatPressed:

    if (button->getPin() == BUTTON_MORE_PIN)
    {
      tuneUp();
    }
    if (button->getPin() == BUTTON_LESS_PIN)
    {
      tuneDown();
    }
    break;
  case AceButton::kEventReleased:
    if (button->getPin() == BUTTON_SET_PIN)
    {
      if (mode == MODE_REGULATOR)
      {
        if (serial)
          Serial.println("prise en compte de Kp, Ki, Kd");
        myPID.SetTunings(conf.Kp, conf.Ki, conf.Kd);
      }
    }
    break;
  }
  // display all
  updateDisplay();
}

void tuneUp()
{
  unsigned long duration = millis() - tuneStart;
  double ratio = 1 + (0.00001f * 4 * duration);
  *value = *value * ratio;
  if (*value > *max)
  {
    *value = *max;
  }
  if (serial)
    Serial.println(*value);
}

void tuneDown()
{
  unsigned long duration = millis() - tuneStart;
  double ratio = 1 - (0.00001f * 4 * duration);
  *value = *value * ratio;
  if (*value < *min)
  {
    *value = *min;
  }
  if (serial)
    Serial.println(*value);
}
